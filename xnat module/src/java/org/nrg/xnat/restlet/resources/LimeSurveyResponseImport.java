package org.nrg.xnat.restlet.extensions;
 
import org.nrg.transaction.TransactionException;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.model.XnatExperimentdataShareI;
import org.nrg.xdat.model.XnatProjectdataI;
import org.nrg.xdat.om.*;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.om.base.BaseXnatExperimentdata;
import org.nrg.xdat.om.ExtLimesurveyassessment;
import org.nrg.xdat.om.base.BaseXnatSubjectdata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.MaterializedView;
import org.nrg.xft.event.EventMetaI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.event.persist.PersistentWorkflowUtils.EventRequirementAbsent;
import org.nrg.xft.exception.InvalidValueException;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xft.utils.StringUtils;
import org.nrg.xft.utils.ValidationUtils.ValidationResults;
import org.nrg.xnat.archive.Rename;
import org.nrg.xnat.archive.Rename.DuplicateLabelException;
import org.nrg.xnat.archive.Rename.FolderConflictException;
import org.nrg.xnat.archive.Rename.LabelConflictException;
import org.nrg.xnat.archive.Rename.ProcessingInProgress;
import org.nrg.xnat.archive.ValidationException;
import org.nrg.xnat.exceptions.InvalidArchiveStructure;
import org.nrg.xnat.helpers.merge.ProjectAnonymizer;
import org.nrg.xnat.helpers.xmlpath.XMLPathShortcuts;
import org.nrg.xnat.restlet.actions.FixScanTypes;
import org.nrg.xnat.restlet.actions.PullSessionDataFromHeaders;
import org.nrg.xnat.restlet.actions.TriggerPipelines;
import org.nrg.xnat.restlet.util.XNATRestConstants;
import org.nrg.xnat.utils.WorkflowUtils;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.Variant;
import org.xml.sax.SAXException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.xnat.restlet.XnatRestlet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.restlet.resource.StringRepresentation;
import org.xml.sax.SAXParseException;
import org.nrg.xnat.restlet.resources.SubjAssessmentAbst;
 
@XnatRestlet("/services/extensions/importlsresponse")
public class LimeSurveyResponseImport  extends SubjAssessmentAbst {
    private static final Log _log = LogFactory.getLog(VerifyExtensionsRestlet.class);
    private static final String PRIMARY = "primary";
 
    protected XnatProjectdata project = null;
    protected XnatSubjectdata subject = null;
    protected String subID = null;
    protected ExtLimesurveyassessment response = null;
    protected XnatSubjectdata existing = null;
 
    public LimeSurveyResponseImport(Context context, Request request, Response response) {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.APPLICATION_JSON));
    }
 
 
    @Override
    public Representation getRepresentation(Variant variant) {
        return new StringRepresentation("Nothing to see here.");
    }
 
    @Override
    public boolean allowPut() {
        return true;
    }
 
    @Override
    public void handlePut() {
        //Only allow limesurvey user, as this API omits some security checks
	if (!user.getUsername().equals("limesurvey")) {
             this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Only Limesurvey user can do this. You are " + user.getUsername());
             return;
        }
        try {
            XFTItem template = null;
            if (existing != null) {
                template = existing.getItem();
            }
 
            XFTItem item = this.loadItem("ext:limesurveyAssessment", true, (this.isQueryVariableFalse("loadExisting")) ? null : template);
 
            if (item == null) {
                item = XFTItem.NewItem("ext:limesurveyAssessment", null);
            }
 
            if (item.instanceOf("ext:limesurveyAssessment")) {
                //Create response Item
                response = (ExtLimesurveyassessment)BaseElement.GetGeneratedItem(item);
 
                //Get project from response and check if it exists
                if (response.getProject() != null) {
                    //Omit user parameter, so that limesurvey user can access all projects
                    project = XnatProjectdata.getXnatProjectdatasById(response.getProject(), null, false);
                } else {
                    this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Submitted limeurvey response must include the project attribute.");
                    return;
                }
                if(this.project == null){
                    this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Project not found.");
                    return;
                }

                //Get subject from response and check if it exists
                if(response.getSubjectId()!=null && !response.getSubjectId().equals("")){
                    //Omit user parameter, so that limesurvey user can access all projects
                    this.subject=XnatSubjectdata.getXnatSubjectdatasById(response.getSubjectId(), null, false);
                } else {
                    this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Submitted limeurvey response must include the subject attribute.");
                }
                if(this.subject==null){
                    this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Subject not found.");
                    return;
                }

                //Set ID for response
                response.setId(ExtLimesurveyassessment.CreateNewID());

                //run XNAT validation checks
                final ValidationResults vr = response.validate(); 
                if (vr != null && !vr.isValid())
                {
                    this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,vr.toFullString());
                    return;
                }

                PersistentWorkflowI wrk= WorkflowUtils.buildOpenWorkflow(user, response.getItem(),newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.getAddModifyAction(response.getXSIType(), (existing==null))));
                EventMetaI c=wrk.buildEvent();
                boolean allowDataDeletion=false;
                if(this.getQueryVariable("allowDataDeletion")!=null && this.getQueryVariable("allowDataDeletion").equals("true")){
                    allowDataDeletion=true;
                }
                try {
                    //Everything worked! Now save the response
                    if(SaveItemHelper.authorizedSave(response,null,true,allowDataDeletion,c)){
                        WorkflowUtils.complete(wrk, c);
                        user.clearLocalCache();
                        MaterializedView.DeleteByUser(user);
                    }
                } catch (Exception e1) {
                        WorkflowUtils.fail(wrk, c);
                        throw e1;
                }

                postSaveManageStatus(response);
 
                this.returnString(response.getId() + ", " + response.getSubjectId() + ", " + response.getProject(),Status.SUCCESS_CREATED);
 
            }
        } catch (SAXParseException e) {
            this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY, e.getMessage());
        } catch (InvalidValueException e) {
            this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
        } catch (Exception e) {
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
        }
    }
}
