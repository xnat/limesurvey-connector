package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author Phillip Thelen <thelen@mpib-berlin.mpg.de>
 *
 * Slightly modified class originally generated by XNAT.
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ExtLimesurveyassessment extends BaseExtLimesurveyassessment {

	public ExtLimesurveyassessment(ItemI item)
	{
		super(item);
	}

	public ExtLimesurveyassessment(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseExtLimesurveyassessment(UserI user)
	 **/
	public ExtLimesurveyassessment()
	{}

	public ExtLimesurveyassessment(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

    public XnatProjectdata getPrimaryProject(boolean preLoad){
        if (this.getProject()!=null){
        	//omit the user, so that limesurvey user can access all data
            return (XnatProjectdata)XnatProjectdata.getXnatProjectdatasById(getProject(), null, preLoad);
        }else{
            return (XnatProjectdata)getFirstProject();
        }
    }

    private XnatSubjectdata subject = null;
    public XnatSubjectdata getSubjectData()
	{
	    if (subject==null)
	    {
            if (getSubjectId()!=null)
            {
            	//omit the user, so that limesurvey user can access all data
                ArrayList al = XnatSubjectdata.getXnatSubjectdatasByField("xnat:subjectData/ID",this.getSubjectId(),null,false);
                if (al.size()>0)
                {
                    subject = (XnatSubjectdata)al.get(0);
                }
            }
	    }
	    return subject;
	}

}

