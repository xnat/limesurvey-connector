# Limesurvey XNAT connector

## What does it do?

The LimeSurvey connector automatically sends filled out surveys to XNAT and stores them as an assessment for a subject. The responses are stores as a native XNAT data type.

## How do I use it?

### XNAT

You need a project with all the subjects, that will participate in the study. 

### LimeSurvey

1. Open *Survey Properties* -> *General Settings* -> *Plugins*
2. Check the checkbox, that survey data should be sent to XNAT
3. Set the project ID of the XNAT project
4. Open *Token Managment* -> *Manage additional attribute fields* and add a new attribute for the subject ID
5. Set the subject ID for each participant
6. As soon as participants fill out the survey, the data will be sent to XNAT

## How do I install it?

### XNAT

1. Clone the repository

		git clone https://mosel.cbs.mpg.de/xnat/limesurvey-connector.git

2. Create a zip file for the XNAT module

		cd xnat\ module
		zip -r limesurveyConnector.zip src

3. Move the zip file to the modules directory in you *xnat_root*
4. Stop tomcat

        sudo service tomcat7 stop

5. Update and redeploy XNAT

		./bin/update.sh -Ddeploy=true

6. Apply the database updates

		psql -U xnat -d xnat -f sql/xnat-update.sql

7. Start tomcat again

        sudo service tomcat7 stop

8. Create a user with the username *limesurvey* in the XNAT admin interface.
9. Activate the *limesurvey* user. The user does not need any additional permissions.
9. Go to the *Data Types* page in the Administration area and select "setup additional data type" and set up the ext:limesurveyAssessment data type

### LimeSurvey

1. Clone the repository

		git clone https://mosel.cbs.mpg.de/xnat/limesurvey-connector.git
		
2. Create a folder called Xnat in the plugins directory of your LimeSurvey installation
3. Move the Xnat.php from the *limesurvey plugin* directory in the repository to the *plugins/Xnat directory* in your LimeSurvey installation
4. Install php curl

		sudo apt-get install curl libcurl3 libcurl3-dev php5-curl 

4. Go to the plugins admin page in LimeSurvey
5. Activate the plugin
6. Set the XNAT url and the username and password for the limesurvey user

    **Note:** The xnat url has to point to the root of your xnat installation. This usually includes */xnat/*. It also should include if you are using *http* or *https* as protocol. 