<?php
ini_set('display_errors', 1);
error_reporting(~0);
    /**
     * This plugin exports response data to a xnat server, after a survey is completed by a user
     *
     * @author Phillip Thelen <thelen@mpib-berlin.mpg.de>
     * @license http://opensource.org/licenses/MIT MIT
     *
     */
    class Xnat extends PluginBase
    {
        static protected $description = 'Xnat exporter: Export completed survey responses to XNAT';
        static protected $name = 'XNAT exporter';

        /**
         * Plugin settings
         */
        protected $settings = array(
            'xnatServer' => array(
                'type' => 'string',
                'label' => 'XNAT Server URL'
            ),
            'xnatUsername' => array(
                'type' => 'string',
                'label' => 'XNAT Limesurvey Username'
            ),
            'xnatPassword' => array(
                'type' => 'string',
                'label' => 'XNAT Limesurvey Password'
            ),
           );
        protected $storage = 'DbStorage';

        public function __construct(PluginManager $manager, $id) {
            parent::__construct($manager, $id);

            // Provides survey specific settings.
            $this->subscribe('beforeSurveySettings');

            // Export data for completed responses
            $this->subscribe('afterSurveyComplete');

            // Save new settings
            $this->subscribe('newSurveySettings');
        }

        /**
         * This event is fired after the survey has been completed.
         * @param PluginEvent $event
         */
        public function afterSurveyComplete() {
            $event = $this->getEvent();
            //Only run if export was enabled
            if (!$this->get('xnatexportenabled', 'Survey', $event->get('surveyId'))) {
                return;
            }
            //make sure we actually have a response id from limesurvey
            if ($event->get('responseId') == null)
            {
                return;
            }

            //Gather survey metadata
            $iSurveyID   = $event->get('surveyId');
            $survey = Survey::model()->findByPk($iSurveyID);
            $surveyName = $survey->getLocalizedTitle();
            $sBaseLanguage = $survey->language;
            $projectID = $this->get('xnatprojectid', 'Survey', $iSurveyID);
            $adminmail = $survey->adminemail;
            $response = $this->pluginManager->getAPI()->getResponse($iSurveyID, $event->get('responseId'));
            $responseID = $event->get('responseId');
            $token = $response["token"];
            $tokenInstance = Token::model($iSurveyID)->findByAttributes(array('token' => $token));
            $subjectID = $tokenInstance->attribute_1;
            $surveyUrl = App()->createAbsoluteUrl("admin/survey/sa/view/surveyid/{$iSurveyID}");
            $aGrouplist = QuestionGroup::model()->getGroups($iSurveyID);

            $xnatDataString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<ext:limesurveyAssessment project=\"$projectID\" label=\"$surveyName - Response $responseID\" xmlns:arc=\"http://nrg.wustl.edu/arc\" xmlns:ext=\"http://nrg.wustl.edu/ext\" xmlns:val=\"http://nrg.wustl.edu/val\" xmlns:pipe=\"http://nrg.wustl.edu/pipe\" xmlns:wrk=\"http://nrg.wustl.edu/workflow\" xmlns:scr=\"http://nrg.wustl.edu/scr\" xmlns:xdat=\"http://nrg.wustl.edu/security\" xmlns:cat=\"http://nrg.wustl.edu/catalog\" xmlns:prov=\"http://www.nbirn.net/prov\" xmlns:xnat=\"http://nrg.wustl.edu/xnat\" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
<xnat:project>$projectID</xnat:project>
<xnat:subject_ID>$subjectID</xnat:subject_ID>
<ext:survey_url>$surveyUrl</ext:survey_url>
<ext:administrator_email>$adminmail</ext:administrator_email>
<ext:response_groups>";

            //add response groups with responses to data string
            LimeExpressionManager::StartProcessingPage(true, Yii::app()->baseUrl);
            foreach ($aGrouplist as $iGID => $aGroup)
            {
                //Response Group
                $groupName = $aGroup["group_name"];
                $xnatDataString .= "<ext:response_group>
<ext:response_group_text>$groupName</ext:response_group_text>
<ext:resps>";
                LimeExpressionManager::StartProcessingGroup($aGroup['gid'], false, $iSurveyID);

                $oQuestionData = Question::model()->getQuestions($iSurveyID, $aGroup['gid'], $sBaseLanguage);

                $qs = array();
                $junk = array();

                foreach ($oQuestionData->readAll() as $q)
                {
                    //Question with response
                    LimeExpressionManager::ProcessString($q['question'], $q['qid']);
                    $question = LimeExpressionManager::GetLastPrettyPrintExpression();
                    $gid = $aGroup['gid'];
                    $questionID = $q["title"];
                    $questionType = $q["type"];
                    $qResponse = $response[$questionID];
                    $xnatDataString .= "<ext:resp>
<ext:question_key>$questionID</ext:question_key>
<ext:question_text>$question</ext:question_text>
<ext:question_type>$questionType</ext:question_type>
<ext:response>$qResponse</ext:response>
<ext:sqs>";
                    $subQuestions = Question::model()->getSubQuestions($q["qid"]);
                    foreach ($subQuestions->readAll() as $sq) {
                        //Subquestion/option with response
                        $subquestionKey = $sq["title"];
                        $subquestion = $sq["question"];
                        $subquestionType = $sq["type"];
                        $sqResponse = $response[$subquestionKey];
                        $xnatDataString .= "<ext:sq>
<ext:subquestion_key>$subquestionKey</ext:subquestion_key>
<ext:subquestion_text>$subquestion</ext:subquestion_text>
<ext:subquestion_type>$questionType</ext:subquestion_type>
<ext:response>$sqResponse</ext:response></ext:sq>";
                    }
                    $xnatDataString .= "</ext:sqs></ext:resp>";
                }
                $xnatDataString .= "</ext:resps>
</ext:response_group>";
                LimeExpressionManager::FinishProcessingGroup();
            }
            LimeExpressionManager::FinishProcessingPage();

            //configure curl
            $xnatDataString .= "</ext:response_groups>
</ext:limesurveyAssessment>";
            $xnatUrl = $this->get('xnatServer');
            if (substr($xnatUrl, -1) != '/') {
                $xnatUrl .= "/";
            }
            $requestUrl = $xnatUrl . "data/services/extensions/importlsresponse/";
            $ch = curl_init();
            $putData = tmpfile();
            $putString = stripslashes($xnatDataString);
            fwrite($putData, $putString);
            fseek($putData, 0);
            curl_setopt($ch, CURLOPT_URL, $requestUrl);
            curl_setopt($ch, CURLOPT_PUT, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
            curl_setopt($ch, CURLOPT_INFILE, $putData);
            curl_setopt($ch, CURLOPT_INFILESIZE, strlen($putString));
            curl_setopt($ch, CURLOPT_USERPWD, $this->get('xnatUsername') . ":" . $this->get('xnatPassword'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

            //send data via curl
            $output = curl_exec($ch);
            if( ! $output) {
                $sFrom = Yii::app()->getConfig("siteadminname") . " <" . Yii::app()->getConfig("siteadminemail") . ">";
                $sSiteName = Yii::app()->getConfig('sitename');
                $sSubject = "[" . $sSiteName . "]Error when exporting response to XNAT";
                $sSiteAdminBounce = Yii::app()->getConfig('siteadminbounce');
                $body = "There was an error when saving a response for the survey " . $surveyName . "\n\nError: ";
                $body = $body . curl_error($ch) . "\n\nThe data that could not be saved:\n";
                $body = $body . $putString;
                SendEmailMessage($body, $sSubject, $adminmail, $sFrom, $sSiteName, false, $sSiteAdminBounce);
            }
            // Close the file
            fclose($putData);
            // Stop curl
            curl_close($ch);
        }



        public function beforeSurveySettings() {
            $event = $this->event;
            $settings = array(
                'name' => get_class($this),
                'settings' => array(
                    'xnatexportenabled' => array(
                        'type' => 'boolean',
                        'label' => 'Export responses for this survey to XNAT',
                        'current' => $this->get('xnatexportenabled', 'Survey', $event->get('survey'))
                    ),

                    'xnatprojectid' => array(
                        'label' => 'Project ID',
                        'type' => 'string',
                        'current' => $this->get('xnatprojectid', 'Survey', $event->get('survey'))
                    )
                )
             );
            //add our survey settings to settings ui
            $event->set("surveysettings.{$this->id}", $settings);

        }

        public function newSurveySettings() {
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value)
        {
            //add all custom settings to survey settings
            $this->set($name, $value, 'Survey', $event->get('survey'));
        }
    }

    }


?>